# Howto

###Initial setup
```
bash <(curl -ks https://gitlab.com/ricochen/dotvim/raw/master/setup.sh)
```

### Update
```
bash <(curl -ks https://gitlab.com/ricochen/dotvim/raw/master/setup.sh) update
```

# Credits

[https://github.com/nicknisi/vim-workshop/blob/master/vimrc](https://github.com/nicknisi/vim-workshop/blob/master/vimrc)

[https://github.com/junegunn/vim-plug](https://github.com/junegunn/vim-plug)

[https://github.com/Valloric/YouCompleteMe#mac-os-x-super-quick-installation](https://github.com/Valloric/YouCompleteMe#mac-os-x-super-quick-installation)

# Note
Original .vim and/or .vimrc will be renamed to <original_name>.YYYYMMDD.HHIISS if exist.

Recommended jshint settings (~/.jshintrc, to allow comma-first coding style)

```
{
	"laxcomma": true
}
```

To compile YCM with support for C-family languages:
```
	cd ~/.vim/plugged/YouCompleteMe
	./install.sh --clang-completer
```

Use Ctrl-e to choose item in the auto complete dropdowns.
