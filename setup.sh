#!/usr/bin/env bash
if [ $# -gt 0 -a "$1" == "update" ]; then
	echo "Updating .vimrc"
	curl -kfLo ~/.vimrc https://gitlab.com/ricochen/dotvim/raw/master/vimrc.vim-plug
	vim -c 'PlugInstall | PlugClean | q | q'
else
	afix=`date '+.%Y%m%d.%H%M%S'`
	[ -e ~/.vim ] && mv ~/.vim{,$afix} > /dev/null 2>&1
	[ -e ~/.vimrc ] && mv ~/.vimrc{,$afix} > /dev/null 2>&1
	curl -kfLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	curl -kfLo ~/.vimrc https://gitlab.com/ricochen/dotvim/raw/master/vimrc.vim-plug
	echo "Installing plugins, YouCompleteMe might take a little bit of time to install"
	vim -c 'PlugInstall | q | q'
	echo "Compiling YouCompleteMe.."
	cd ~/.vim/plugged/YouCompleteMe
	./install.sh
fi
